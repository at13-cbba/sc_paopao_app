FROM openjdk:11-jre
EXPOSE 8080
WORKDIR /app
RUN apt-get update && apt install ffmpeg -y && apt install tesseract-ocr -y && apt install libtesseract-dev -y && apt install nodejs -y && apt install npm -y
COPY ./build/libs/*.jar /app/converter-0.0.1-SNAPSHOT.jar
COPY ./.env.develop /app/.env.develop
COPY ./archive /app/archive
COPY ./tessdata /app/tessdata
ENTRYPOINT ["java", "-jar", "converter-0.0.1-SNAPSHOT.jar"]
